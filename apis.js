var trip_BASE_URL = "https://www.chiikinogennki.soumu.go.jp/k-cloud-api/v001/kanko/"
var deta_html = new String()

function kanko_deta(place_type, place_code) {
    var url = trip_BASE_URL + place_type + "/json?code=" + place_code + "&limit=50";
    console.log(url)
    $.ajax({
        url: url,
        type: 'GET',
        dataType: "json",
        success: function (data) {
            console.log(data);
        },
        error: function (data) {
            console.log(data.status);
        }
    })
}; 

function zone_kanko_deta(place_type, place_code) {
    // place_zoneは数字
    switch (place_code) {
        case 0:
            // ゾーン設定部
            // 沼津都市圏Area
            // 沼津市,三島市,函南町,清水町,長泉町,伊豆の国市
            // ここのCity_Codeに全国地方公共団体コードの5桁をリストとして記入
            var City_Code = ["22203", "22206", "22325", "22341", "22342", "22225"]
            for (var i = 0; i < City_Code.length; i++) {
                var deta = kanko_deta(place_type, City_Code[i]);
            }
            break;
        case 1:
            // ゾーン設定部
            // ここのCity_Codeに全国地方公共団体コードの5桁をリストとして記入
            var City_Code = ["22203", "22206", "22325", "22341", "22342", "22225"]
            for (var i = 0; i < City_Code.length; i++) {
                var deta = kanko_deta(place_type, City_Code[i]);
            }
            break;
        case 2:
            // ゾーン設定部
            // ここのCity_Codeに全国地方公共団体コードの5桁をリストとして記入
            var City_Code = ["22203", "22206", "22325", "22341", "22342", "22225"]
            for (var i = 0; i < City_Code.length; i++) {
                var deta = kanko_deta(place_type, City_Code[i]);
            }
            break
        case 3:
            // ゾーン設定部
            // ここのCity_Codeに全国地方公共団体コードの5桁をリストとして記入
            var City_Code = ["22203", "22206", "22325", "22341", "22342", "22225"]
            for (var i = 0; i < City_Code.length; i++) {
                var deta = kanko_deta(place_type, City_Code[i]);
            }
            break
        case 4:
            // ゾーン設定部
            // ここのCity_Codeに全国地方公共団体コードの5桁をリストとして記入
            var City_Code = ["22203", "22206", "22325", "22341", "22342", "22225"]
            for (var i = 0; i < City_Code.length; i++) {
                var deta = kanko_deta(place_type, City_Code[i]);
            }
            break
        default:
            break;
    }
}

function kanko_deta_fanc(Type_code, place_type, place_code) {
    switch (Type_code) {
        case 0:
            kanko_deta(place_type, place_code);
            break;
        case 1:
            zone_kanko_deta(place_type, place_code);
            break;
        default:
            break;
    }
}



window.onload = function () {
    //川越市(11201)の史跡の情報を表示する場合
    kanko_deta_fanc(0,"史跡","11201");
    //指定したゾーン番号「0」の史跡の情報を表示する場合
    kanko_deta_fanc(1,"史跡",0);
}